import React from "react";
import { createRoot } from "react-dom/client";
import rem from 'dylan-rem';
import "./index.less";
import App from "./App";

rem.init({
  designWidth: 375,
  rootValue: 37.5,
  maxRatio: 2
});

const rootElement = document.getElementById("root");
const root = createRoot(rootElement);
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
